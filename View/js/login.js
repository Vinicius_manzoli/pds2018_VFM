$(document).ready(function() {
$("#login").click(function()
{
	email = $("#email").val();
    senha = $("#senha").val();
    
	var testevazio = true;

	if(!$("#email").val() || !$("#senha").val()){
		M.toast({html: 'Preencha todos os campos', classes: 'rounded red'});
		testevazio = false;
	}

	if(testevazio == true){
	  $.ajax({
	      url: "../Controller/Login.php",
	      type: 'POST',
	      data: { 	
                    'email' : email,
					'senha' : senha},

	      	success: function(result) {
	          window.location.href = "../View/Home.html";
	        },
	        error:function(error){
	        	M.toast({html: 'Email ou senha incorretos', classes: 'rounded red'});
        }
	  	});
	}
	return false;
});
});
