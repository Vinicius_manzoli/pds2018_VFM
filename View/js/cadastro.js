$("#cadastrar").click(function()
{	
	nome = $("#nome").val();
	email = $("#email").val();
    senha = $("#senha").val();
    
	var testevazio = true;
	if($("#senha").val() != $("#senha_confirmacao").val()){
		testevazio = false;
		Materialize.toast('Senhas não são iguais!', 1500, 'rounded red');
	}

	if(!$("#nome").val() || !$("#email").val() || !$("#senha").val()){
		Materialize.toast('Preencha todos os campos!', 1500, 'rounded red');
		testevazio = false;
	}

	else if ($("#email").val().indexOf("@") == -1 ||
      $("#email").val().indexOf(".") == -1 ||
      $("#email").val() == "" ||
      $("#email").val() == null) {
      	testevazio = false;
      	Materialize.toast('Por favor, indique um e-mail válido!', 1500, 'rounded red');
        $("#email").focus();

    } 

	if(testevazio == true){
	  $.ajax({
	      url: "../Controller/Cadastro.php",
	      type: 'POST',
	      data: { 	
                    'nome' : nome,
                    'email' : email,
					'senha' : senha},

	      success: function(result) {
	          window.location.href = "../View/index.html";
	          cod = jQuery.parseJSON(result);
	        },
	        error:function(error){
	       	if ( error.responseText.trim() == "email")
	        {
	        	Materialize.toast('Email já utilizado!', 1500, 'rounded red');
	        }
        }
	  	});
	}
    return false;
});
